//
//  AppDelegate.swift
//  Brute Patches
//
//  Created by Ware, Adam @ Brisbane on 16/1/20.
//  Copyright © 2020 AdamWare.me. All rights reserved.
//

import UIKit
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        return true
    }

}

